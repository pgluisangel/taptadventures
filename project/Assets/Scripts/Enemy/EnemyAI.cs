using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyEnums;

public class EnemyAI : MonoBehaviour {
    public bool debug;
    enum State {
        Idle,
        Attack,
        Block,
    }

    AttackTypes lastAttack;
    State state;
    private EnemyController enemy;
    private int comboCounter;
    string currentAnimation;

    // WeightedActions
    WeightedAction attackLeftToRight;
    WeightedAction attackRightToLeft;
    WeightedAction attackUpToDown;

    void Awake() {
        lastAttack = AttackTypes.Fumble;
        comboCounter = 0;
        enemy = GetComponent<EnemyController>();
        CreateWeightedActions();

        StartCoroutine(FSM());
    }

    void CreateWeightedActions() {
        attackLeftToRight = new WeightedAction(0.33f);
        attackLeftToRight.key = AttackTypes.LeftToRight.ToString();
        attackLeftToRight.onAction += () => {
            lastAttack = AttackTypes.LeftToRight;
            enemy.animator.SetTrigger("LeftToRight");
        };

        attackRightToLeft = new WeightedAction(0.33f);
        attackRightToLeft.key = AttackTypes.RightToLeft.ToString();
        attackRightToLeft.onAction += () => {
            lastAttack = AttackTypes.RightToLeft;
            enemy.animator.SetTrigger("RightToLeft");
        };

        attackUpToDown = new WeightedAction(0.34f);
        attackUpToDown.key = AttackTypes.UpToDown.ToString();
        attackUpToDown.onAction += () => {
            lastAttack = AttackTypes.UpToDown;
            enemy.animator.SetTrigger("UpToDown");
        };
    }

    IEnumerator FSM() {
        while (true) 
            yield return StartCoroutine(state.ToString());
    }

    IEnumerator Idle() {
        float rndWait = Random.Range(enemy.stats.basic.BehaviourChangeMinDelay, enemy.stats.basic.BehaviourChangeMaxDelay);
        if (debug) print("Next action in: " + rndWait);
        yield return new WaitForSeconds(rndWait);
        State newState = GetRandomState();
        ChangeState(newState);
    }

    IEnumerator Attack() {
        List<WeightedAction> actions = new List<WeightedAction>();
        actions.Add(attackLeftToRight);
        actions.Add(attackRightToLeft);
        actions.Add(attackUpToDown);
        DoRandomWeightedAction(actions);

        yield return 0;
        while (!enemy.animator.GetBool("CheckCombo")) yield return 0;
        enemy.animator.SetBool("CheckCombo", false);

        if (!CheckCombo()) {
            ChangeState(State.Idle);
            comboCounter = 0;
            yield return new WaitForSeconds(enemy.animator.GetCurrentAnimatorClipInfo(0).Length * (1 - enemy.animator.GetCurrentAnimatorStateInfo(0).normalizedTime));
        }

    }

    IEnumerator Block() {
        enemy.animator.SetTrigger("Block");
        yield return 0;
        yield return new WaitForSeconds(enemy.animator.GetCurrentAnimatorClipInfo(0)[0].clip.length);
        ChangeState(State.Idle);
    }
 
    void ChangeState(State newState) {
        if (debug) print("Change state to: " + newState);
        state = newState;
    }

    State GetRandomState() {
        float roll = Random.Range(0f, 1f);
        float accumulation = 0f;

        if (CheckChance(roll, enemy.stats.basic.AttackChance, ref accumulation)) return State.Attack;
        if (CheckChance(roll, enemy.stats.basic.BlockChance, ref accumulation)) return State.Block;

        return State.Idle;
    }

    bool CheckChance(float roll, float chance, ref float accumulation) {
        float finalChance = chance + accumulation;
        accumulation += chance;
        
        return roll <= finalChance;
    }

    bool CheckCombo() {
        float comboChance = enemy.stats.basic.AttackComboChance - (enemy.stats.basic.AttackComboChanceDecrease * comboCounter);
        float roll = Random.Range(0f, 1f);

        if (roll <= comboChance) {
            comboCounter += 1;
            return true;
        }

        return false;
    }

    void DoRandomWeightedAction(List<WeightedAction> actions) {
        float roll = Random.Range(0f, 1f);
        float accumulation = 0f;
        float finalChance = 0f;
        WeightedAction removeAction = actions.Find((removeAction) => removeAction.key.Equals(lastAttack.ToString()));
        if (removeAction != null) {
            accumulation += removeAction.weight;
            actions.Remove(removeAction);
        }

        foreach (WeightedAction action in actions) {
            finalChance = action.weight + accumulation;
            if (roll <= finalChance) {
                action.ExecuteAction();
                return;
            }
            accumulation += action.weight;
        }
    }
}


class WeightedAction {
    public WeightedAction(float weight) {
        this.weight = weight;
    }
    public string key;
    public float weight;
    public delegate void Action();
    public event Action onAction;
    public void ExecuteAction() {
        onAction();
    }
}