using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyEnums;

public class EnemyController : MonoBehaviour {

    private EnemyAI ai;
    public EnemyStatsController stats;
    public Animator animator;

    private void Awake() {
        ai =  GetComponent<EnemyAI>();
        stats = GetComponent<EnemyStatsController>();
        animator = GetComponent<Animator>();
    }

    public void AttackPlayer(AttackTypes attack) {
        GameManager.Instance.player.RecieveAttack(attack);
    }

}
