using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyLifeBarController : MonoBehaviour {
    private EnemyStatsController enemyStats;
    private Image barFill;

    private void Start() {
        enemyStats = GameManager.Instance.enemy.stats;
        barFill = GetComponent<Image>();
    }
    
    public void UpdateLifeBar() {
        float percentageLife = enemyStats.health / enemyStats.basic.Health;
        barFill.rectTransform.localScale = new Vector3(percentageLife, 1f, 1f);
    }
}
