using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStatsController : MonoBehaviour {
    public EnemyStats basic;
    public float health;

    private void Awake() {
        health = basic.Health;
    }

    public void RecieveDamage(int damage) {
        if (health <= 0) return;

        health -= damage;
        GameManager.Instance.ui.enemyLife.UpdateLifeBar();
    }
}
