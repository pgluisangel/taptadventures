namespace MyEnums {
    public enum AttackTypes {
        Fumble,
        LeftToRight,
        RightToLeft,
        UpToDown,
    }

    public enum PlayerStates {
        Idle,
        DashRight,
        DashLeft,
        DashBack
    }
}
