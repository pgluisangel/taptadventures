using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager Instance;
    public PlayerController player;
    public EnemyController enemy;
    public UIController ui;

    private void Awake() {
        if (Instance != null && Instance != this) Destroy(gameObject);
        else {
            GameSettings();
            GetReferences();

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void GetReferences() {
        player = FindObjectOfType<PlayerController>();
        enemy = FindObjectOfType<EnemyController>();
        ui = FindObjectOfType<UIController>();
    }

    private void GameSettings() {
        // Application.targetFrameRate = 60;
        // QualitySettings.vSyncCount = 0;
    }

    public void PlayerDeath() {

    }

    public void EnemyDeath() {
        
    }
}
