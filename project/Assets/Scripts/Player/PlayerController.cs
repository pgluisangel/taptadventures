using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyEnums;

public class PlayerController : MonoBehaviour {
    public PlayerStatsController stats;
    private PlayerActions actions;
    public Animator animator;

    private void Awake() {
        animator = GetComponentInParent<Animator>();
    }

    public void RecieveAttack(AttackTypes attack) {
        int playerState = animator.GetInteger("State");
        
        if ((int) attack != playerState) {
            stats.Damage(1);
        }
    }

    public void DoDamage(int baseDamage) {
        GameManager.Instance.enemy.stats.RecieveDamage(baseDamage);
    }
}
