using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatsController : MonoBehaviour {
    public PlayerStats basic;

    public float health;

    private void Awake() {
        health = basic.Health;
    }

    public void Damage(int damage) {
        if (health <= 0) return;

        health -= damage;
        GameManager.Instance.ui.playerLife.UpdateUI();

        if (health <= 0) GameManager.Instance.PlayerDeath();
    }
}
