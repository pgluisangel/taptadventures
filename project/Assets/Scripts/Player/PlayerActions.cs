using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour {
    private PlayerController player;

    private float attackResetDelay;

    private void Awake() {
    }

    private void Start() {
        player = GameManager.Instance.player;
        ResetAttackTimerAnimation();
    }

    public void SingleTap() {
        if (IsInputBlocked()) return;

        player.animator.SetBool("IsAttacking", true);
        ResetAttackTimerAnimation();

        player.DoDamage(1);
    }

    public void SwipeUp() {
        if (IsInputBlocked()) return;
        player.animator.SetTrigger("HeavyAttack");

        player.DoDamage(2);
    }
    public void SwipeRight() {
        if (IsInputBlocked()) return;
        player.animator.SetTrigger("DashRight");
    }

    public void SwipeDown() {
        if (IsInputBlocked()) return;
        player.animator.SetTrigger("DashBack");
    }

    public void SwipeLeft() {
        if (IsInputBlocked()) return;
        player.animator.SetTrigger("DashLeft");
    }

    private bool IsInputBlocked() {
        AnimatorStateInfo stateInfo = player.animator.GetCurrentAnimatorStateInfo(0);
        AnimatorStateInfo nextStateInfo = player.animator.GetNextAnimatorStateInfo(0);
        return stateInfo.IsTag("AnimBlock") || nextStateInfo.IsTag("AnimBlock");
    }

    private void ResetAttackTimerAnimation() {
        attackResetDelay = player.stats.basic.AttackResetDelay;
    }

    private void StopAttack() {
        player.animator.SetBool("IsAttacking", false);
    }

    private void Update() {
        if (attackResetDelay > 0) {
            attackResetDelay -= Time.deltaTime;
            if (attackResetDelay <= 0) StopAttack();
        }
    }
}
