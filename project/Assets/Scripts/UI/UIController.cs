using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {
    public EnemyLifeBarController enemyLife;
    public UIPlayerLifes playerLife;

    private void Awake() {
        enemyLife = GetComponentInChildren<EnemyLifeBarController>();
        playerLife = GetComponentInChildren<UIPlayerLifes>();
    }
}
