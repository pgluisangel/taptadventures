using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerLifes : MonoBehaviour {

    public GameObject uiLifePrefab;
    public Sprite heartFull;
    public Sprite heartEmpty;
    private HorizontalLayoutGroup lifesGroup;
    private PlayerController player;
    private List<Image> lifes;

    private void Awake() {
        lifes = new List<Image>();
        lifesGroup = GetComponent<HorizontalLayoutGroup>();
    }

    private void Start() {
        player = GameManager.Instance.player;

        for (int i = 0; i < player.stats.basic.Health; i++) {
            GameObject uiLife = Instantiate(uiLifePrefab, transform);
            lifes.Add(uiLife.GetComponent<Image>());
        }
    }

    public void UpdateUI() {
        Image targetUILife = lifes[(int) player.stats.health];
        targetUILife.sprite = heartEmpty;
    }
}
