using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVFX : MonoBehaviour {

    public GameObject vfxParent;
    public GameObject slashVFX;

    public void SpawnSlashVFX(string parent) {
        Transform parentTransform = vfxParent.transform.Find(parent);
        GameObject slash = Instantiate(slashVFX, parentTransform.transform);
        // slash.transform.localPosition = new Vector3(0.43f, 0.71f, 0.82f);
        // slash.transform.eulerAngles = new Vector3(0f, -65.96f, 0f);
        // slash.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
    }
}
