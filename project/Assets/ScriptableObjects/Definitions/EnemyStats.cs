using UnityEngine;

[CreateAssetMenu(fileName = "EnemyStats", menuName = "ScriptableObjects/EnemyStats", order = 2)]
public class EnemyStats : ScriptableObject {
    public float Health = 100;
    public float AttackDamage = 1.0f;
    public float BehaviourChangeMinDelay = 0.5f;
    public float BehaviourChangeMaxDelay = 1.5f;

    public float AttackChance = 0.5f;
    public float IdleChance = 0.25f;
    public float BlockChance = 0.25f;

    public float AttackComboChance = 0.3f;
    public float AttackComboChanceDecrease = 0.05f;
}