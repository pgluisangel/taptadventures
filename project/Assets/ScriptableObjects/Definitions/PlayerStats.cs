using UnityEngine;

[CreateAssetMenu(fileName = "PlayerStats", menuName = "ScriptableObjects/PlayerStats", order = 1)]
public class PlayerStats : ScriptableObject {
    public int Health = 3;
    public float AttackDamage = 1.0f;
    public float AttackResetDelay = 0.2f; // If this time passes without attack input, player will automatically block

}